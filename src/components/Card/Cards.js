import React, { Component } from "react";

import Card from './Card';
// import resort from './alexander.jpg';
// import {Link} from 'react-router-dom';

import './Cards.css';

class Cards extends Component {
    state = {
        cards : [],
    }

    componentWillMount(){
      this.setState({cards:this.props.CardData});
    }

     render () {
        let cards = <p >something went wrong!</p>
        if(!this.state.error){
            cards = this.state.cards.map(card => {
                return (
                        //  <Link to={'/' + post.id} key={post.id} >
                            
                            
                            <Card                               
                            key={card.id}
                            tourName = {card.productName}
                            price={card.price} 
                            text={card.text}
                            source={card.imagesrc}
                            lightName={this.props.lightName}
                            />
                        // </Link>
                            )
            });
        }
         return(
            <section className="box">
            {cards}
            </section>
            
         );
     }
}


export default Cards;