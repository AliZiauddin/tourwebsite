import React from 'react';
// import Cards from './components/Card/Cards';

import TourPage from './components/TourPage/TourPage';
import RestaurantPage from './components/RestaurantPage/RestaurantPage';
import FaqPage from './components/FaqPage/FaqPage';

import './App.css';

function App() {
  return (
    <div className="App">
      {/* <TourPage/> */}
      {/* <RestaurantPage/> */}
      <FaqPage/>
    </div>
  );
}

export default App;
